#include <iostream>

class Animal
{
public:
	virtual void Voice()
	{
		std::cout << "Animal voice!" << std::endl;
	}
};

class Dog : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Woof!" << std::endl;
	}
};

class Cat : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Meow!" << std::endl;
	}
};

class Cow : public Animal
{
public:
	void Voice() override
	{
		std::cout << "Moo!" << std::endl;
	}
};

int main() {
	Animal* Animals[3];
	Animals[0] = new Dog();
	Animals[1] = new Cat();
	Animals[2] = new Cow();

	for (int i = 0; i < 3; i++) {
		Animals[i]->Voice();
	}

	for (int i = 0; i < 3; i++) {
		delete Animals[i];
	}

	return 0;
}
